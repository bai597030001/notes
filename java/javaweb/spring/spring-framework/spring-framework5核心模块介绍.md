[spring framework5核心模块介绍](https://docs.spring.io/)

# 1.spring framework5核心模块介绍

- 组成 Spring 框架的每个模块集合或者模块都可以单独存在,也可以一个或多个模块联

## 1.1核心容器

- 由 spring-beans、spring-core、spring-context 和 spring-expression(SpringExpression Language, SpEL) 4 个模块组成。
- spring-beans 和 spring-core 模块是 Spring 框架的核心模块,包含了控制反转(Inversion ofControl, IOC)和依赖注入(Dependency Injection, DI)。

## 1.2AOP和设备支持

## 1.3数据访问及集成

## 1.4Web

## 1.5报文发送